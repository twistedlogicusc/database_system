# Database Application - Social Network (Java, JDBC, MapReduce, AWS) #

* Developed an end-to-end java application similar to Facebook with GUI, enabling spatial queries.
* Developed large scale queries with MapReduce and compared performance as allocated machines grow.
![alt tag](https://bytebucket.org/twistedlogicusc/database_system/raw/5d980ff8721c85309b3e79b924fb6a3f2bcf91c8/JavaApplication/585.png)