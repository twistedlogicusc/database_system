create table Address
(
  AID char(15) not null,
  Street_no char(15),
  street_add char(40),
  city char(30),
  state char(4),
  country char(50),
  zip INTEGER,
  vertice MDSYS.SDO_GEOMETRY,
  primary key (AID)
);
insert into user_sdo_geom_metadata values('ADDRESS','vertice',SDO_DIM_ARRAY(SDO_DIM_ELEMENT('x',0,100,1),SDO_DIM_ELEMENT('Y',0,100,1)),NULL);
create index index_ADDRESS on ADDRESS(vertice) indextype is MDSYS.SPATIAL_INDEX; 

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('1', '655', 'W Jefferson', 'Los Angeles', 'CA', 'Uinted States', 90007.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(460, 434, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('2', '8446', 'Melrose Pl', 'Los Angeles', 'CA', 'Uinted States', 90069.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(165, 195, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('3', '1300', 'E Main St', 'Alhambra', 'CA', 'Uinted States', 91801.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(770, 165, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('4', '3607', 'Trousdale Pkwy', 'Los Angeles', 'CA', 'Uinted States', 90089.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(450, 445, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('5', '5757', 'Wilshire Blvd', 'Los Angeles', 'CA', 'Uinted States', 90036.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(310, 277, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('6', '1133', ' N La Brea Ave', 'West Hollywood', 'CA', 'Uinted States', 90038.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(333, 157, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('7', '3335', 'S Figueroa St.', 'Los Angeles', 'CA', 'Uinted States', 90007.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(470, 430, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('8', '2400', 'Broadway ', 'Santa Monica', 'CA', 'Uinted States', 90404.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(45, 470, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('9', '2', 'Griffith Park', 'Los Angeles', 'CA', 'Uinted States', 90027.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(530, 345, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('10', '405', 'Hilgard Avenue', 'Los Angeles', 'CA', 'Uinted States', 90095.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(128, 246, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('11', '1200', '1200 12th Avenue, Los Angeles, CA', 'Los Angeles', 'CA', 'Uinted States', 90019.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(372, 334, NULL), null, null));

INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, vertice) 
VALUES ('12', '3300', 'Wilshire Blvd', 'Los Angeles', 'CA ', 'Uinted States', 90010.0, SDO_GEOMETRY(2001, null, SDO_POINT_TYPE(429, 280, NULL), null, null));


create table users
(
 email char(50),
 Passwd char(10),
 Fname char(30),
 Lname char(30),
 Birthdate date,
 AID char(15) not null,
  primary key (email),
  foreign key (AID) references Address on delete cascade
  
);


INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('angelina@csci585.edu', '123456', 'Angelina', 'Jolie', to_date('06/04/1975', 'mm/dd/yyyy'), '1');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('brad@csci585.edu', '123456', 'Brad', 'Pitt', to_date('12/18/1963', 'mm/dd/yyyy'), '1');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('al@csci585.edu', '123456', 'Alfredo', 'Pacino', to_date('04/25/1970', 'mm/dd/yyyy'), '3');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('steve@csci585.edu', '123456', 'Steve', 'Jobs', to_date('02/24/1955', 'mm/dd/yyyy'), '2');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('bill@csci585.edu', '123456', 'Bill', 'Gates', to_date('10/28/1955', 'mm/dd/yyyy'), '2');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('miley@csci585.edu', '123456', 'Miley', 'Cyrus', to_date('11/23/1992', 'mm/dd/yyyy'), '6');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('hope@csci585.edu', '123456', 'Hope', 'Solo', to_date('07/20/1981', 'mm/dd/yyyy'), '2');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('andre@csci585.edu', '123456', 'Andre', 'Agassi', to_date('04/29/1970', 'mm/dd/yyyy'), '8');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('nelle@csci585.edu', '123456', 'Nelle', 'Lee', to_date('09/12/1955', 'mm/dd/yyyy'), '2');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('jen@csci585.edu', '123456', 'Jennifer', 'Aniston', to_date('02/11/1969', 'mm/dd/yyyy'), '5');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('pit@csci585.edu', '123456', 'Pit', 'Sampras', to_date('08/12/1971', 'mm/dd/yyyy'), '5');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('josh@csci585.edu', '123456', 'Josh', 'Radnor', to_date('07/29/1974', 'mm/dd/yyyy'), '5');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('tom@csci585.edu', '123456', 'Tom', 'Cruise', to_date('07/03/1962', 'mm/dd/yyyy'), '4');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('serina@usc.edu', '123456', 'Serena', 'Williams', to_date('09/26/1981', 'mm/dd/yyyy'), '4');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('venus@csci585.edu', '123456', 'Venus', 'Williams', to_date('06/17/1989', 'mm/dd/yyyy'), '3');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('fan@csci585.edu', '123456', 'Fan', 'Bingbing', to_date('09/16/1981', 'mm/dd/yyyy'), '7');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('jay@csci585.edu', '123456', 'Jay', 'Chou', to_date('01/18/1979', 'mm/dd/yyyy'), '7');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('jakie@csci585.edu', '123456', 'Jackie', 'Chan', to_date('04/07/1954', 'mm/dd/yyyy'), '6');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('andy@csci585.edu', '123456', 'Andy', 'Lau', to_date('09/27/1961', 'mm/dd/yyyy'), '10');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('zzhang@csci585.edu', '123456', 'Zhang', 'Ziyi', to_date('02/09/1979', 'mm/dd/yyyy'), '8');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('haung@usc.edu', '123456', 'Haung', 'Xiaoming', to_date('11/13/1977', 'mm/dd/yyyy'), '10');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('son@csci585.edu', '123456', 'Yeon-Jae', 'Son', to_date('05/28/1994', 'mm/dd/yyyy'), '9');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('kim@csci585.edu', '123456', 'Soo-Hyun', 'Kim', to_date('02/16/1988', 'mm/dd/yyyy'), '9');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('urm@csci585.edu', '123456', 'Urmila ', 'Matondkar', to_date('02/04/1974', 'mm/dd/yyyy'), '9');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('kamal@csci585.edu', '123456', 'Kamal', 'Hassan', to_date('11/07/1954', 'mm/dd/yyyy'), '11');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('mani@csci585.edu', '123456', 'Manisha ', 'Koirala', to_date('08/16/1970', 'mm/dd/yyyy'), '11');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('niki@csci585.edu', '123456', 'Niki', 'Karimi', to_date('11/10/1971', 'mm/dd/yyyy'), '3');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('bbahram@csci585.edu', '123456', 'Bahram', 'Radan', to_date('04/28/1979', 'mm/dd/yyyy'), '11');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('had@csci585.edu', '123456', 'Hadis', 'Fouladvand', to_date('01/01/1979', 'mm/dd/yyyy'), '12');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('tina@csci585.edu', '123456', 'Tina', 'Verma', to_date('10/22/1992', 'mm/dd/yyyy'), '6');

INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) 
VALUES ('deena@csci585.edu', '123456', 'Deena', 'Gandhi', to_date('02/04/1989', 'mm/dd/yyyy'), '12');






create table Friend
(
user1 char(50) not null,
user2 char(50) not null,
FTYPE char(40),
 primary key (user1,user2),
foreign key (user1) references users on delete cascade,
  foreign key (user2) references users on delete cascade
);


INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('angelina@csci585.edu', 'brad@csci585.edu', 'Family');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('angelina@csci585.edu', 'al@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('angelina@csci585.edu', 'nelle@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('angelina@csci585.edu', 'zzhang@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('angelina@csci585.edu', 'niki@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('brad@csci585.edu', 'nelle@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('brad@csci585.edu', 'hope@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('brad@csci585.edu', 'haung@usc.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('al@csci585.edu', 'andre@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('steve@csci585.edu', 'bill@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('steve@csci585.edu', 'tina@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('serina@usc.edu', 'venus@csci585.edu', 'Family');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('andre@csci585.edu', 'fan@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('nelle@csci585.edu', 'had@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('nelle@csci585.edu', 'jen@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('pit@csci585.edu', 'venus@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('josh@csci585.edu', 'jakie@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('josh@csci585.edu', 'nelle@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('josh@csci585.edu', 'kamal@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('josh@csci585.edu', 'niki@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('josh@csci585.edu', 'son@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('serina@usc.edu', 'tom@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('jay@csci585.edu', 'andy@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('son@csci585.edu', 'tom@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('urm@csci585.edu', 'kim@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('niki@csci585.edu', 'bbahram@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('niki@csci585.edu', 'had@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('bbahram@csci585.edu', 'tina@csci585.edu', 'Close Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('son@csci585.edu', 'kim@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('deena@csci585.edu', 'mani@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('miley@csci585.edu', 'pit@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('jen@csci585.edu', 'jakie@csci585.edu', 'Regular Friend');

INSERT INTO FRIEND (USER1, USER2, FTYPE) 
VALUES ('bbahram@csci585.edu', 'angelina@csci585.edu', 'Regular Friend');


create table Event
(
  Title char(40) not null,
  Description char(500),
  AID char(15) not null,
  stime date,
  dur char(20),
  primary key (Title),
  foreign key (Aid) references address
);




INSERT INTO EVENT (TITLE, DESCRIPTION, AID, STIME, DUR) 
VALUES ('Tailgate 13', 'Football & Tailgate Tickets USC Trojans vs. UTAH Utes Saturday, October 26th. Join us for a tailgate three hours prior to kickoff', '5', to_date('10/26/2013', 'mm/dd/yyyy'), '2 Hours');

INSERT INTO EVENT (TITLE, DESCRIPTION, AID, STIME, DUR) 
VALUES ('Superbowl 13', 'The Super Bowl is the annual championship game of the National Football League (NFL), the highest level of professional American football in the United States, culminating a season that begins in the late summer of the previous calendar year.', '4', to_date('12/15/2013', 'mm/dd/yyyy'), '4 Hours');

INSERT INTO EVENT (TITLE, DESCRIPTION, AID, STIME, DUR) 
VALUES ('NBAFinals 13', 'The NBA Finals is the championship series of the National Basketball Association. The series was named the NBA World Championship Series until 1986.', '3', to_date('01/15/2013', 'mm/dd/yyyy'), '3 Hours');

INSERT INTO EVENT (TITLE, DESCRIPTION, AID, STIME, DUR) 
VALUES ('Oscars 13', 'The Academy Awards, now officially known as The Oscars,[1] are a set of awards given annually for excellence of cinematic achievements. The Oscar statuette is officially named the Academy Award of Merit and is one of nine types of Academy Awards.', '3', to_date('11/11/2013', 'mm/dd/yyyy'), '5 Hours');

INSERT INTO EVENT (TITLE, DESCRIPTION, AID, STIME, DUR) 
VALUES ('US OPEN 13', 'The United States Open Tennis Championships is a hardcourt tennis tournament which is the modern iteration of one of the oldest tennis championships in the world, the U.S. National Championship, for which men''s singles was first contested in 1881.', '2', to_date('09/10/2013', 'mm/dd/yyyy'), '14 Days');






create table POST
(
  PID char(40) not null,
  Note char(500),
  sender char(50) not null,
  Plevel integer,
  pdate date,
  primary key (PID),
  foreign key (sender) references users on delete cascade
);




INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('1', '"What''s something that adults do that they shouldn''t do?"
"Punish kids. And war."', 'angelina@csci585.edu', 2.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('2', '"I''m from the Ivory Coast."
"Why''d you come to America?"
He formed his hands into two imaginary guns. "Too much boom boom," he said. "So I run."', 'angelina@csci585.edu', 1.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('3', '"What was the happiest moment of your life?"
"Any time I wake up and nothing hurts."', 'brad@csci585.edu', 2.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('4', '"Everyone loves me. I go everywhere, and I don''t fight with nobody. If somebody''s yelling at me, I just keep walking. I''ve only been in one fight. I used to be a taxi driver. ', 'nelle@csci585.edu', 1.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('5', '"I had this idea to make a portrait, and instead of filling it in with physical features, I filled it in with the verbs that each body part can do."', 'niki@csci585.edu', 2.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('6', '"My dad is big and strong and lifts the heavy weights at the gym. He also is a fireman and he once saved eleven people."', 'bbahram@csci585.edu', 1.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('7', '"If you could give one piece of advice to a large group of people, what would it be?"
"Be optimistic."', 'had@csci585.edu', 2.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('8', 'Today in microfashion...', 'kim@csci585.edu', 1.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('9', '"If you could give one piece of advice to a large group of people, what would it be?"
"Enjoy it while it lasts."', 'zzhang@csci585.edu', 0.0, to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) 
VALUES ('10', '"I''m breaking out of my shell."', 'andy@csci585.edu', 1.0, to_date('09/14/2013', 'mm/dd/yyyy'));





create table postcomm
(
  PID char(40) not null,
  Text char(500),
  mid char(50) not null,
  commdate date,
  primary key (PID, Text, mid, commdate),
  foreign key (mid) references users on delete cascade,
  foreign key (pid) references post on delete cascade
  );
  
  


INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'GREAT!', 'andre@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'GREAT!', 'andre@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'GREAT!', 'andre@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'GREAT!', 'andre@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'GREAT!', 'brad@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'AWESOME!', 'brad@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'AWESOME!', 'brad@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'AWESOME!', 'brad@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'AWESOME!', 'steve@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'AWESOME!', 'steve@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'AMAZING!', 'steve@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'AMAZING!', 'steve@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'AMAZING!', 'niki@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'AMAZING!', 'niki@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'AMAZING!', 'niki@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'AMAZING!', 'niki@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'REALLY?', 'haung@usc.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'REALLY?', 'haung@usc.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'REALLY?', 'haung@usc.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'REALLY?', 'haung@usc.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('1', 'REALLY?', 'kamal@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('3', 'REALLY?', 'kamal@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('5', 'COOL!', 'kamal@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('7', 'COOL!', 'kamal@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('2', 'COOL!', 'al@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('4', 'COOL!', 'had@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('6', 'COOL!', 'angelina@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('8', 'COOL!', 'son@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('10', 'COOL!', 'jay@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));

INSERT INTO POSTCOMM (PID, TEXT, MID, COMMDATE) 
VALUES ('2', 'COOL!', 'nelle@csci585.edu', to_date('09/14/2013', 'mm/dd/yyyy'));




  
create table friendreq
(
  sender char(50),
  receiver char(50),
  ftype char(40),
  primary key (sender, receiver),
  foreign key (sender) references users on delete cascade,
  foreign key (receiver) references users on delete cascade
);


