package assignment2;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.util.ArrayList;


class LoginPanel extends JPanel{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextField username;
	JPasswordField password;
	JButton login;
	JButton signup;
	private String UserName;
	private String Password;

	public void disablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].disable();
		}
	}

	public void enablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].enable();
		}
	}

	LoginPanel(){
		this.setBounds(810, 30, 250, 100);
		this.setLayout(new GridLayout(3,2));
		this.add(new Label("UserName: "));
		username = new JTextField();
        this.add(username);
		this.add(new Label("Password: "));
		password = new JPasswordField();
        this.add(password);
        login = new JButton("Login");
        signup = new JButton("Signup");
        this.add(login);
        this.add(signup);
        }
	
	
	public String getUserName(){
		UserName = username.getText();
		return UserName;
	}
	
	public String getPassword(){
		Password = password.getText();
		return Password;
	}
	public void setUserName(String user){
		UserName=user;
	}
	
	public void clearPassword(){
		Password="";
	}
	
	public void clearUserName(){
		UserName="";
	}
	
}

class SignupPanel extends JPanel{
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JButton login;
	JButton signup;
	JPasswordField password;
	JPasswordField password2;

	JTextField country;
	JTextField state;
	JTextField city;
	JTextField email;
	JTextField birthday;
	JTextField fname;
	JTextField lname;
	JTextField str_no;
	JTextField str_address;
	JTextField zip;

	
	public void disablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].disable();
		}
		disableButton();
	}

	public void enablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].enable();
		}
		enableButton();
	}

	SignupPanel(){
		this.setBounds(810, 150, 250, 300);
		this.setLayout(new GridLayout(11,2));
		this.add(new Label("Email: "));
		email = new JTextField();
        this.add(email);
		this.add(new Label("Password: "));
		password = new JPasswordField();
        this.add(password);
		this.add(new Label("ReEnter Password: "));
		password2 = new JPasswordField();
        this.add(password2);
		this.add(new Label("First Name: "));
		fname = new JTextField();
        this.add(fname);
		this.add(new Label("Last Name: "));
		lname = new JTextField();
        this.add(lname);
        this.add(new Label("Birthday : "));
		birthday = new JTextField();
        this.add(birthday);
        this.add(new Label("City: "));
		city = new JTextField();
        this.add(city);
		this.add(new Label("strNo : "));
		str_no = new JTextField();
        this.add(str_no);
		this.add(new Label("strAdress : "));
		str_address = new JTextField();
        this.add(str_address);
        this.add(new Label("Zip : "));
		zip = new JTextField();
        this.add(zip);
        this.add(new Label(""));
        signup = new JButton("signup");
        this.add(signup);
        disablePanel();
	}

	public void disableButton(){
		signup.setEnabled(false);
	}
	public void enableButton(){
		signup.setEnabled(true);
	}
}

class SqlPanel extends JPanel{
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextArea SQLArea = null;
	JScrollPane scrollPane = null;
	JLabel showLabel;
	SqlPanel(){
		setInputArea();
	}

	public void disablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].disable();
		}
	}

	public void enablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].enable();
		}
	}

	private void setInputArea(){
		setBounds(0, 495,810, 150);
		SQLArea = new JTextArea(6,68);
		SQLArea.setLineWrap(true);
		scrollPane = new JScrollPane(SQLArea);
		this.add(scrollPane);
	}
}

class Frame0 extends JFrame{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextField txtfield;
	JButton btn1;
	String lbltext;
	JLabel label;
	String FriendType[] = { "Normal Friend", "Close Friend", "Family" };
	JComboBox combo;

	Frame0(String text,int mode){
		this.lbltext = text;
		 this.setSize(300, 120);
		 this.setResizable(false);
		setLayout(new FlowLayout());
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		setLocation((width-this.getWidth())/2,(height-this.getHeight())/2);
		label = new JLabel(text);
		add(label);
		txtfield = new JTextField(10);
        add(txtfield);
        combo= new JComboBox(FriendType);
        if(mode==1)
        	add(combo);
        btn1 = new JButton("OK");
        add(btn1);

	}
}

class Frame1 extends JFrame {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextField txtfield;
	JButton btn1;
	JButton btn2;
	String lbltext0;
	String lbltext1;
	JLabel lbl0;
	JLabel lbl1;
	JTextArea textArea = null;

	Frame1(String text0, String text1) {
		this.lbltext0 = text0;
		this.lbltext1 = text1;
		this.setResizable(false);
		setLayout(null);
		 this.setSize(300, 256);
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		setLocation((width-this.getWidth())/2,(height-this.getHeight())/2);
		lbl0 = new JLabel(text0);
		add(lbl0);
		lbl0.setBounds(10, 10, 70, 30);

		txtfield = new JTextField(10);
		add(txtfield);
		txtfield.setBounds(85, 10, 150, 30);

		lbl1 = new JLabel(text1);
		add(lbl1);
		lbl1.setBounds(10, 45, 70, 30);

		textArea = new JTextArea(8, 10);
		textArea.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane);
		scrollPane.setBounds(85,55,180,90);

		btn1 = new JButton("OK");
		btn1.setBounds(90,165,90, 30);
		add(btn1);

	}
}


class Frame2 extends JFrame {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextField txtfield;
	JButton btn1;
	JButton btn2;
	String lbltext0;
	JLabel lbl0;

	Frame2(String text0) {
		this.setResizable(false);
		this.lbltext0 = text0;
        this.setSize(300, 150);
        this.setResizable(false);
		setLayout(null);

		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		setLocation((width-this.getWidth())/2,(height-this.getHeight())/2);
		lbl0 = new JLabel(text0);
		add(lbl0);
		lbl0.setBounds(10, 10, 70, 30);

		txtfield = new JTextField(10);
		add(txtfield);
		txtfield.setBounds(85, 10, 100, 30);

		btn1 = new JButton("Decline");
		btn2 = new JButton("Accept ALL");

		btn1.setBounds(190,10,90, 30);
		btn2.setBounds(70,60,150, 30);
		add(btn1);
		add(btn2);

	}
}


class Frame3 extends JFrame {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextField []txtfield;
	JButton btn1;
	JLabel []lbl;
	Frame3(String text0) {
		this.setResizable(false);
		this.setTitle(text0);
        this.setSize(1000, 70);
        this.setResizable(false);
		setLayout(new GridLayout(1,9));

		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		setLocation((width-this.getWidth())/2,(height-this.getHeight())/2);
		lbl = new JLabel[4];
		for(int i=0;i<4;i++)
			lbl[i] = new JLabel();
		lbl[0].setText("topleft x:");
		lbl[1].setText("topleft y:");
		lbl[2].setText("bottomright x:");
		lbl[3].setText("bottomright y:");

		txtfield = new JTextField[4];
		for(int i=0;i<4;i++)
			txtfield[i] = new JTextField();

		btn1 = new JButton("search");
		for(int i=0;i<4;i++)
		{
			add(lbl[i]);
			add(txtfield[i]);
		}
		add(btn1);
	}
}


class PostandSearchPanel extends JPanel{
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextArea textArea = null;
	JScrollPane scrollPane = null;

	JTextField text;
	JLabel showLabel;
	JButton []buttons;

	public void disableButton(){
		for(int i=0;i<buttons.length;i++)
			buttons[i].setEnabled(false);
	}

	public void enableButton(){
		for(int i=0;i<buttons.length;i++)
			buttons[i].setEnabled(true);
	}

	PostandSearchPanel(){
		setLayout(null);
		this.setBounds(0, 10,780, 120);
        setInputArea();

		buttons = new JButton[2];
		buttons[0]= new JButton("Post");
		buttons[1]= new JButton("Search For Friend");

		text  = new JTextField(15);
		text.setLocation(10,3);
		text.setText("");
		text.setBounds(460, 40, 150, 25);
		buttons[0].setBounds(380,40,60,25);
		buttons[1].setBounds(620,40,130,25);

		
		this.add(buttons[0]);
		this.add(text);
		this.add(buttons[1]);

	}
	public void disablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].disable();
		}
		disableButton();
	}

	public void clearPanel(){
		text.setText("");
	    textArea.setText("");
	}

	public  void enablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].enable();
		}
		enableButton();
	}

	public void setInputArea(){
		textArea = new JTextArea(4,30);
		textArea.setLineWrap(true);
		textArea.setBounds(10, 10, 360, 90);
		this.add(textArea);
	}
 }

class ResultPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextArea resultArea = null;
	JScrollPane scrollPane = null;
	ResultPanel(){
		setResultArea();
	}

	public void setResultArea(){
		resultArea = new JTextArea(10,30);
		resultArea.setLineWrap(true);
		resultArea.setBounds(10, 140,750, 250);

		scrollPane = new JScrollPane(resultArea);
		add(scrollPane);

	}
}

class ButtonPanel extends JPanel{
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	JTextArea textArea = null;
	JScrollPane scrollPane = null;
	JLabel showLabel;
	JButton []buttons;

	ButtonPanel(){
	  this.setBounds(10, 415, 770, 40);
	 	buttons = new JButton[8];
		this.setLayout(new GridLayout(2,4));
        for(int i=0;i<8;i++){
        	buttons[i]= new JButton();
        	this.add(buttons[i]);
        }
        buttons[0].setText("Add Friend");
        buttons[1].setText("List all posts");
        buttons[2].setText("List all comments on a post");
        buttons[3].setText("Comment on A post");
        buttons[4].setText("List all events");
        buttons[5].setText("Friend request");   
        buttons[6].setText("Find nearest friend");
        buttons[7].setText("Range query");

	}

	public void disableButton(){
		for(int i=0;i<buttons.length;i++)
			buttons[i].setEnabled(false);
	}

	public void enableButton(){
		for(int i=0;i<buttons.length;i++)
			buttons[i].setEnabled(true);
	}

	public void disablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
				comps[i].disable();
		}
		disableButton();
	}

	public void enablePanel(){
		if(this.getComponentCount()==0)
			return ;
		Component[] comps= new Component[this.getComponentCount()];
		comps = this.getComponents();
		for(int i=0;i<comps.length;i++){
			comps[i].enable();
		}
		enableButton();
	}
}


class MainFrame extends JFrame{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	String sqlAll = "";
	JLabel LogoLabel;
	JButton notifyButton;
	LoginPanel loginPanel;
	SignupPanel signUpPanel;
	SqlPanel sqlPanel;
	ButtonPanel buttonPanel;
	PostandSearchPanel postandsearch;
	ResultPanel resultPanel;
	Connection conn=null;
	ArrayList<String> requester = new ArrayList<String>();
	ArrayList<String> Relation= new ArrayList<String>();
	int countrequest=0;
	JTextArea resultArea = null;
	JScrollPane scrollPane = null;
	int trigger = 0;
	JLabel showLabel;
	int hasRequest = 0;
	StringBuffer SQLOut = new StringBuffer ();
	MainFrame(){
		setResizable(false);
		setLayout(null);
		setSize(1100, 700);
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		setLocation((width-1100)/2,(height-700)/2);
		setTitle("This is GUI for database homework");
		setNotifyLabel();
		SetLogo();
		setLoginPanel();
		setSignupPanel();
		setSqlPanel();
		setButtonPanel();
		setPostandSearchPanel();
		setResultPanel();
		postandsearch.disablePanel();
		buttonPanel.disablePanel();
	}

	public void disableResult(){
    	resultArea.setText("");
    	resultArea.setEditable(false);
    	resultArea.setEnabled(false);
    	scrollPane.setEnabled(false);
	}

	public void setResultPanel(){
		resultArea = new JTextArea(10,30);
		resultArea.setLineWrap(true);
		scrollPane = new JScrollPane(resultArea);
		add(scrollPane);
		scrollPane.setBounds(10, 140,770, 250);
	}

	public void SetLogo(){
	Image image;
	try {
		image = ImageIO.read(new File("usc_viterbi_logo.jpg"));
		ImageIcon icon = new ImageIcon(image);
		LogoLabel = new JLabel();
		LogoLabel.setIcon(icon);
		LogoLabel.setBounds(830,480,300,150);

		add(LogoLabel);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  //this generates an image file

	}

	public void setNotifyLabel(){
		Image image;
		try {
			image = ImageIO.read(new File("notify.png"));
			ImageIcon icon = new ImageIcon(image);
			notifyButton = new JButton();
			notifyButton.setIcon(icon);
			notifyButton.setBounds(750,3,30,30);

			notifyButton.addActionListener(new ActionListener() {
	           
	            public void actionPerformed(ActionEvent e) {
	            	/*Fill this function*/
	            	/*Press this notification button, you should list all the friend request on the ResultPanel*/
	            	/*the output format is like "... wants to add you as 'friend type'." */
	            	ConnectDB connect = new ConnectDB();
	            	Connection conn = connect.openConnection();
	            	try{
	            		Statement stmt = conn.createStatement();
	            		String sql = "select f.sender, f.ftype "
	            				+ "from friendreq f where f.receiver ='" + loginPanel.getUserName() + "'";
	            		ResultSet rslt = stmt.executeQuery(sql);
	            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
	            		String result = "";

	            		while (rslt.next()){

		    	    		result += rslt.getString(1).trim() + "  wants to add you as " + rslt.getString(2).trim() + "\n";
		    	    	}
	            		resultArea.setText(result);
	            	         
	            	}
	            	catch(SQLException e1){
	            	     e1.printStackTrace();          
	            	}
	            	finally{
	            	     try{
	            	          if (conn != null){
	            	               conn.close();
	            	          }
	            	     }
	            	     catch(SQLException e1){
	            	          e1.printStackTrace();          
	            	     }
	            	}
	            	
	            	
	            	
	            }
	        });

			add(notifyButton);
			notifyButton.setVisible(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  //this generates an image file


	}

	public void setButtonPanel(){
		buttonPanel = new ButtonPanel();
		this.add(buttonPanel);

		buttonPanel.buttons[0].addActionListener(new ActionListener() {
           
            public void actionPerformed(ActionEvent e) {
            	final Frame0 frame=new Frame0("User Email",1);
                frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                frame.setVisible(true);

                frame.btn1.addActionListener(new ActionListener() {
                    
                    public void actionPerformed(ActionEvent e) {
                    	/*Fill this function*/
    	            	/*Press this add friend button, input user Email information, press OK, you should be able to send friend request*/
    	            	/*After press ok, you should also pop up a standard dialog box to show the request send status <succeed or failed>*/
                    	ConnectDB connect = new ConnectDB();
                    	Connection conn = connect.openConnection();
                    	boolean found = false;
                    	try{
                    		Statement stmt = conn.createStatement();
                    	    String emailSrch = frame.txtfield.getText();
                    	    //System.out.println(emailSrch);
                    	    String ftype = (String) frame.combo.getSelectedItem();
                    	    //System.out.println(ftype);
                    	    String sql = "INSERT INTO friendreq (sender, receiver, ftype) VALUES ('" + loginPanel.getUserName() + "', '" + emailSrch + "', '" + ftype + "')";
                    	    ResultSet rslt = stmt.executeQuery (
                	    		      "select EMAIL from USERS"
                	    		    );
                    	    
                    	    while (rslt.next()){
                	    		String email = rslt.getString(1).trim();     	    		
                	    		if (frame.txtfield.getText().equals(email)){
                	    			found = true;
                    	    		break;
                    	    	}
                	    	}
                    	    
                    	    if (found == false){
                    	    	throw new IllegalArgumentException();
                    	    }
                    	    else{
                    	    	if (!frame.txtfield.getText().equals(loginPanel.getUserName())){
		                    	    stmt.executeQuery (sql); 
		                    	    sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
		                    	    JOptionPane.showMessageDialog(null, "Success! Friend request has been sent to " + frame.txtfield.getText());
                    	    	}
                    	    	else{
                    	    		throw new IllegalStateException();
                    	    	}
                    	    }
                    	}
                    	catch(SQLException e1){
                    		JOptionPane.showMessageDialog(null, "You cannot request twice!");
                    	    e1.printStackTrace();          
                    	}
                    	catch(IllegalArgumentException e1){
                    		JOptionPane.showMessageDialog(null, frame.txtfield.getText() + "----Invalid email!");
                    	}
                    	catch(IllegalStateException e1){
                    		JOptionPane.showMessageDialog(null, "You cannot request to yourself!");
                    	}
                    	finally{
                    	     try{
                    	          if (conn != null){
                    	               conn.close();
                    	          }
                    	     }
                    	     catch(SQLException e1){
                    	          e1.printStackTrace();          
                    	     }
                    	}

                    }
                });

            }
        });
		buttonPanel.buttons[1].addActionListener(new ActionListener() {
          
            public void actionPerformed(ActionEvent e) {
            	/*Fill this function*/
            	/*Press this list all post button, you should be able to list all the post which are visible to you*/
            	/*You can define the output format*/
            	ConnectDB connect = new ConnectDB();
            	Connection conn = connect.openConnection();
            	try{
            		Statement stmt = conn.createStatement();
            		String sql = "select distinct p.pid, p.note, p.sender, p.pdate"
            				+ " from post p, friend f"
            				+ " where (p.plevel = 2) OR ((p.plevel = 1) AND ( ((p.sender = f.user1) AND (f.user2 = '"+loginPanel.getUserName()+"')) OR ((p.sender = f.user2) AND (f.user1 = '"+loginPanel.getUserName()+"')))) OR (p.sender = '"+loginPanel.getUserName()+"')"
            				+ " order by p.pid asc";
            		ResultSet rslt = stmt.executeQuery(sql);
            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
            		String result = "";
            		while (rslt.next()){
            			for (int i = 1;i <= 3 ;i++){
	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "\n";
	    	    		}
            			result = result + rslt.getMetaData().getColumnName(4) + ": " + rslt.getDate(4).toString().trim() + "\n\n";
	    	    		//result += rslt.getString(1).trim() + "\n\n";
	    	    	}
            		resultArea.setText(result);
            		
            		
            	}
            	catch(SQLException e1){
            	     e1.printStackTrace();          
            	}
            	finally{
            	     try{
            	          if (conn != null){
            	               conn.close();
            	          }
            	     }
            	     catch(SQLException e1){
            	          e1.printStackTrace();          
            	     }
            	}
            	
            	
            }
        });

		buttonPanel.buttons[2].addActionListener(new ActionListener() {
          
            public void actionPerformed(ActionEvent e) {
            	final Frame0 frame=new Frame0("Post ID: ",2);
                frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                frame.setVisible(true);
                frame.btn1.addActionListener(new ActionListener() {
                 
                    public void actionPerformed(ActionEvent e) {
                    	/*Fill this function*/
    	            	/*Press this List Comments Button, input Post ID, press OK, you should be able to list all the comment about this post*/
                    	
                    	ConnectDB connect = new ConnectDB();
                    	Connection conn = connect.openConnection();
                    	try{
                    		boolean found = false;
                    		Statement stmt = conn.createStatement();
                    		String sql = "select distinct p.pid, p.note"
                    				+ " from post p, friend f"
                    				+ " where (p.plevel = 2) OR ((p.plevel = 1) AND ( ((p.sender = f.user1) AND (f.user2 = '"+loginPanel.getUserName()+"')) OR ((p.sender = f.user2) AND (f.user1 = '"+loginPanel.getUserName()+"')))) OR (p.sender = '"+loginPanel.getUserName()+"')"
                    				+ " order by p.pid asc";
                    		ResultSet rslt = stmt.executeQuery(sql);
                    		sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
                    		while(rslt.next()){
                    			if(frame.txtfield.getText().equals(rslt.getString(1).trim())){
                    				found = true;
                    				break;
                    			}
                    		}
                    		if (found == false){
                    			throw new NullPointerException();
                    		}
                    		else{
                    			String result = "";
                    			sql = "select distinct p.pid, p.text, p.mid, p.commdate"
                    					+ " from postcomm p"
                    					+ " where p.pid = '"+frame.txtfield.getText()+"'";
                    			rslt = stmt.executeQuery(sql);
                    			sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
                    			while(rslt.next()){
                    				for (int i = 1;i <= 3 ;i++){
            	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
            	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    ";            	    	    			
                    				}
                    				result = result + rslt.getMetaData().getColumnName(4) + ": " + rslt.getDate(4).toString().trim() + "\n";
                    			}
                    			resultArea.setText(result);
                    		}
                    	         
                    	}
                    	catch(SQLException e1){
                    	     e1.printStackTrace();          
                    	}
                    	catch(NullPointerException e1){
                    		//e1.printStackTrace();
                    		JOptionPane.showMessageDialog(null, "You can't read the comment due to the privacy!");		
                    	}
                    	finally{
                    	     try{
                    	          if (conn != null){
                    	               conn.close();
                    	          }
                    	     }
                    	     catch(SQLException e1){
                    	          e1.printStackTrace();          
                    	     }
                    	}
                    	
                    }
                });
            }
        });
		
		buttonPanel.buttons[3].addActionListener(new ActionListener() {
           
            public void actionPerformed(ActionEvent e) {
            	final Frame1 frame=new Frame1("Post ID: ","Content: ");
                frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                frame.setVisible(true);
                frame.btn1.addActionListener(new ActionListener() {
                    
                    public void actionPerformed(ActionEvent e) {
                    	/*Fill this function*/
    	            	/*Press this Comment Button, input Post ID, and comment, press OK, you should be able to comment on this post*/
                    	/*After press ok, you should also pop up a standard dialog box to show the comment's status <succeed or failed>*/	
                    	ConnectDB connect = new ConnectDB();
                    	Connection conn = connect.openConnection();
                    	try{
                    		boolean found = false;
                    		Statement stmt = conn.createStatement();
                    		String sql = "select distinct p.pid, p.note"
                    				+ " from post p, friend f"
                    				+ " where (p.plevel = 2) OR ((p.plevel = 1) AND ( ((p.sender = f.user1) AND (f.user2 = '"+loginPanel.getUserName()+"')) OR ((p.sender = f.user2) AND (f.user1 = '"+loginPanel.getUserName()+"')))) OR (p.sender = '"+loginPanel.getUserName()+"')"
                    				+ " order by p.pid asc";
                    		ResultSet rslt = stmt.executeQuery(sql);
                    		sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
                    		while(rslt.next()){
                    			if(frame.txtfield.getText().equals(rslt.getString(1).trim())){
                    				found = true;
                    				break;
                    			}
                    		}
                    		if (found == false){
                    			throw new NullPointerException();
                    		}
                    		else{
                    			sql = "INSERT INTO postcomm (pid, text, mid, commdate) VALUES ('"+frame.txtfield.getText()+"', '"+frame.textArea.getText()+"', '"+loginPanel.getUserName()+"', to_date('"+new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime())+"', 'mm/dd/yyyy'))";
                    			stmt.executeQuery(sql);
                    			sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
                        		JOptionPane.showMessageDialog(null, "Comment succeeded!");		
                    		}
                    	         
                    	}
                    	catch(SQLException e1){
                    	     e1.printStackTrace();          
                    	}
                    	catch(NullPointerException e1){
                    		//e1.printStackTrace();
                    		JOptionPane.showMessageDialog(null, "You can't post the comment due to the privacy!");		
                    	}
                    	finally{
                    	     try{
                    	          if (conn != null){
                    	               conn.close();
                    	          }
                    	     }
                    	     catch(SQLException e1){
                    	          e1.printStackTrace();          
                    	     }
                    	}
                    	
                    	
                    	
                    }
                });
            }
        });
		
		buttonPanel.buttons[4].addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
            	/*Fill this function*/
            	/*Press this List all event Button, you should be able to list all available event*/
            	
            	ConnectDB connect = new ConnectDB();
            	Connection conn = connect.openConnection();
            	try{
            		Statement stmt = conn.createStatement();
            		String sql = "select * from event";		
            		ResultSet rslt = stmt.executeQuery(sql);
            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
            		String result = "";
            		while (rslt.next()){
            			java.util.Date date = Calendar.getInstance().getTime();
            			int tcomp = rslt.getDate(4).compareTo(date);
	    	    		if (tcomp > 0){
	    	    			for (int i = 1;i <= 3 ;i++){
    	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
    	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "\n";            	    	    			
            				}
            				result = result + rslt.getMetaData().getColumnName(4) + ": " + rslt.getDate(4).toString().trim() + "\n";
            				result = result + rslt.getMetaData().getColumnName(5) + ": " + rslt.getString(5).trim() + "\n\n";
	    	    		}
	    	    	}
            		resultArea.setText(result);
            		
            	         
            	}
            	catch(SQLException e1){
            	     e1.printStackTrace();          
            	}
            	finally{
            	     try{
            	          if (conn != null){
            	               conn.close();
            	          }
            	     }
            	     catch(SQLException e1){
            	          e1.printStackTrace();          
            	     }
            	}
            	
            }
        });


		buttonPanel.buttons[5].addActionListener(new ActionListener() {
           
        public void actionPerformed(ActionEvent e) {
        	final Frame2 frame=new Frame2("Decline: ");
            frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            frame.setVisible(true);

            frame.btn1.addActionListener(new ActionListener() {
               
                public void actionPerformed(ActionEvent e) {
                	/*Fill this function*/
                	/*Press this decline Button, you should be able to decline friend request*/
                	/*You could decline one at a time, or decline many at a time. e.g. using comma to separate each request*/
                	/*pop up a standard dialog box to show <succeed or failed>*/
                	
                	ConnectDB connect = new ConnectDB();
                	Connection conn = connect.openConnection();
                	try{
                		Statement stmt = conn.createStatement();
                		String dec = frame.txtfield.getText();
                		//System.out.println(dec);
                		Scanner decScan = new Scanner(dec).useDelimiter("\\s*,\\s*");
                		while(decScan.hasNext()){
                			dec = decScan.next();
	                		//System.out.println(dec);
			            	String sql =  "delete from friendreq where sender = '" + dec + "'";
			    	    	ResultSet rslt = stmt.executeQuery (sql);
			    	    	sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
			    	    	JOptionPane.showMessageDialog(null, "You have declined " + dec);
                		}
                	         
                	}
                	catch(SQLException e1){
                	     e1.printStackTrace();          
                	}
                	finally{
                	     try{
                	          if (conn != null){
                	               conn.close();
                	          }
                	     }
                	     catch(SQLException e1){
                	          e1.printStackTrace();          
                	     }
                	}
                }
            });


            frame.btn2.addActionListener(new ActionListener() {
           
                public void actionPerformed(ActionEvent e) {
                	/*Fill this function*/
                	/*Press this accept all Button, you should be able to accept all friend request and add this information into friend relationship table*/
                	/*pop up a standard dialog box to show <succeed or failed>*/
                	
                	ConnectDB connect = new ConnectDB();
                	Connection conn = connect.openConnection();
                	try{
                		Statement stmt = conn.createStatement();
	            		String sql = "select * from friendreq f where f.receiver ='" + loginPanel.getUserName() + "'";
	            		ResultSet rslt = stmt.executeQuery(sql);
	            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
	            		//String result = "";
	            		ArrayList<String> user1Arr = new ArrayList<String>();
	            		ArrayList<String> user2Arr = new ArrayList<String>();
	            		ArrayList<String> user3Arr = new ArrayList<String>();
	            		while(rslt.next()){
	            			/*
	            			for (int i = 1;i <= rslt.getMetaData().getColumnCount() ;i++){
	        	    			System.out.println (rslt.getString(i)); // Print col 
	            			}
	            			*/
	            			user1Arr.add(rslt.getString(1).trim());
	            			user2Arr.add(rslt.getString(2).trim());
	            			user3Arr.add(rslt.getString(3).trim());
	            			
	            		}
		            	for(int i = 0; i < user1Arr.size();i++){
		            		String sql1 = "INSERT INTO friend (user1, user2, ftype) VALUES ('" + user1Arr.get(i) + "', '" + user2Arr.get(i) + "', '" + user3Arr.get(i) + "')";
		            		stmt.executeQuery(sql1);
	            			sqlPanel.SQLArea.setText(sqlAll += sql1 + "\n");
	            			JOptionPane.showMessageDialog(null, "Add friend " + user1Arr.get(i));
	            			String sql2 =  "delete from friendreq where sender = '" + user1Arr.get(i) + "'";
	            			stmt.executeQuery(sql2);
	            			sqlPanel.SQLArea.setText(sqlAll += sql2 + "\n");
		            	}
            			
                	}
                	catch(SQLException e1){
                		JOptionPane.showMessageDialog(null, "The first person has already been your friend. You should decline hem/her!");
                	    e1.printStackTrace();   
                	}
                	finally{
                	     try{
                	    	 
                	    	  
                	          if (conn != null){
                	               conn.close();
                	          }
                	     }
                	     catch(SQLException e1){
                	          e1.printStackTrace();          
                	     }
                	}
                	
                }
            });

        }
		});

		buttonPanel.buttons[6].addActionListener(new ActionListener() {
         
            public void actionPerformed(ActionEvent e) {
            	/*Fill this function*/
            	/*Press this Button, you should be able list the information(including address information) about your friend who live nearest to you*/
            	/*This is a spatial query*/
            	ConnectDB connect = new ConnectDB();
            	Connection conn = connect.openConnection();
            	try{
            		
            		String x = "0";
            		String y = "0";
            		String result = "";
            		Statement stmt = conn.createStatement();
            		String sql = "select distinct u.email, u.aid"
            				+ " from users u, friend f"
            				+ " where  ((u.email = f.user1) AND (f.user2 = '"+loginPanel.getUserName()+"')) OR ((u.email = f.user2) AND (f.user1 = '"+loginPanel.getUserName()+"'))";
            		ResultSet rslt = stmt.executeQuery(sql);
            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
            		Map<String, String> fMap = new HashMap<String, String>();
            		Set<String> fSet = fMap.keySet();
            		while (rslt.next()){
            			//System.out.println(rslt.getString(1)+"   "+rslt.getString(2));
            			fMap.put(rslt.getString(1), rslt.getString(2));
            		}
            		/*
            		for(String ks : fSet){
        				
        			     System.out.println(ks);  
            		}
            		*/
            		
            		sql = "select ad.vertice.SDO_POINT.x, ad.vertice.SDO_POINT.Y from address ad, users u where (u.aid = ad.aid) and (u.email = '"+ loginPanel.getUserName() +"')";
            		rslt = stmt.executeQuery(sql);
            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
            		while (rslt.next()){
            			x = rslt.getString(1);
            			y = rslt.getString(2);
            		}
            		
            		sql = "SELECT distinct ad.aid FROM address ad WHERE (SDO_NN(ad.vertice, sdo_geometry(2001, NULL, sdo_point_type("+ x +","+ y +",NULL), NULL, NULL),  'sdo_batch_size=15') = 'TRUE') ";
            		rslt = stmt.executeQuery(sql);
            		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
            		boolean flag = false;
            		while (rslt.next() && (flag == false)){
            			String r = rslt.getString(1);
            			//System.out.println(r);
            			
            			for(String ks : fSet){
            				if(r.equals(fMap.get(ks))){
            			         System.out.println(ks);
            			         result += ks+"\n";
            			         flag = true;
            			    }
            			}
            			//oldVal = r;
            		}
            		Scanner name = new Scanner(result);
            		String ad = name.next();
            		
            		sql = "select ad.vertice.SDO_POINT.x, ad.vertice.SDO_POINT.Y from address ad, users u where (u.aid = ad.aid) and (u.email = '"+ ad +"')";
            		rslt = stmt.executeQuery(sql);
            		//sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
            		while (rslt.next()){
            			x = rslt.getString(1);
            			y = rslt.getString(2);
            		}
            		
            		sql = "SELECT distinct ad.aid, ad.Street_no, ad.street_add, ad.zip, ad.city from address ad, users u where (u.aid = ad.aid) AND (u.email = '"+ ad +"')";
            		rslt = stmt.executeQuery(sql);
            		while (rslt.next()){
            			for (int i = 1;i <= 5 ;i++){
	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "\n";
	    	    		}
	    	    	}
            		result = result + "cordX = " + x + "  " + "cordY =" + y;
            		resultArea.setText(result);
            	}
            	catch(SQLException e1){
            	     e1.printStackTrace();          
            	}
            	finally{
            	     try{
            	          if (conn != null){
            	               conn.close();
            	          }
            	     }
            	     catch(SQLException e1){
            	          e1.printStackTrace();          
            	     }
            	}
            	
            	
            	
            	
            }
        });
		buttonPanel.buttons[7].addActionListener(new ActionListener() {
           
            public void actionPerformed(ActionEvent e) {
            	final Frame3 frame=new Frame3("Please input coordinate: ");
                frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                frame.setVisible(true);

                frame.btn1.addActionListener(new ActionListener() {
                  
                    public void actionPerformed(ActionEvent e) {
                    	/*Fill this function*/
                    	/*Press this Button, input left top corner coordinate and right down corner coordinate*/
                    	/*press ok, you should be able list the information(including address information) about your friend who lives in this area. Close query window*/
                    	/*This is a spatial query*/
                    	frame.setVisible(false);
                    	ConnectDB connect = new ConnectDB();
                    	Connection conn = connect.openConnection();
                    	try{
                    		
                    		String x = "0";
                    		String y = "0";
                    		String result = "";
                    		Statement stmt = conn.createStatement();
                    		String sql = "select distinct u.email, u.aid"
                    				+ " from users u, friend f"
                    				+ " where  ((u.email = f.user1) AND (f.user2 = '"+loginPanel.getUserName()+"')) OR ((u.email = f.user2) AND (f.user1 = '"+loginPanel.getUserName()+"'))";
                    		ResultSet rslt = stmt.executeQuery(sql);
                    		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
                    		Map<String, String> fMap = new HashMap<String, String>();
                    		Set<String> fSet = fMap.keySet();
                    		while (rslt.next()){
                    			//System.out.println(rslt.getString(1)+"   "+rslt.getString(2));
                    			fMap.put(rslt.getString(1), rslt.getString(2));
                    		}
                    		
                    		for(String ks : fSet){
                				
                			     System.out.println(ks + "~");  
                    		}
                    		
                    		

                    		
                    		sql = "SELECT distinct u.email, ad.aid, ad.Street_no, ad.street_add, ad.zip, ad.city, ad.vertice.SDO_POINT.x, ad.vertice.SDO_POINT.y FROM address ad, users u WHERE (sdo_relate(ad.vertice, SDO_geometry(2003,NULL,NULL,SDO_elem_info_array(1,1003,3),SDO_ordinate_array("+frame.txtfield[0].getText()+","+frame.txtfield[3].getText()+", "+frame.txtfield[2].getText()+","+frame.txtfield[1].getText()+")),'MASK=ANYINTERACT') = 'TRUE')  AND (u.aid = ad.aid)";
                    		rslt = stmt.executeQuery(sql);
                    		sqlPanel.SQLArea.setText(sqlAll += sql + "\n"); 
                    		while (rslt.next()){
                    			String r = rslt.getString(1);
                    			//System.out.println(r);
                				if(fSet.contains(r)){
                			         for (int i = 1;i <= 8 ;i++){
             	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
             	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "\n";
             	    	    		}
                			    }
                				result = result + "\n";
                    			//oldVal = r;
                    		}
                    		resultArea.setText(result);
                    	}
                    	catch(SQLException e1){
                    	     e1.printStackTrace();          
                    	}
                    	finally{
                    	     try{
                    	          if (conn != null){
                    	               conn.close();
                    	          }
                    	     }
                    	     catch(SQLException e1){
                    	          e1.printStackTrace();          
                    	     }
                    	}
                    	
                    	
                    }
                });
            }
        });
		
	}

	public void setPostandSearchPanel(){
		postandsearch = new PostandSearchPanel();
		this.add(postandsearch);
		StringBuffer result= new StringBuffer();


		postandsearch.buttons[0].addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) { 
            	/*Fill this function*/
            	/*Press this Button, you should be able post the information written in the textArea*/
            	/*pop up a standard dialog box to show <succeed or failed>*/
            	ConnectDB connect = new ConnectDB();
            	Connection conn = connect.openConnection();
            	try{
            		Statement stmt = conn.createStatement();
	            	String sql =  "select pid from post";
	    	    	ResultSet rslt = stmt.executeQuery (sql);
	    	    	sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
	    	    	int num = 1;
	    	    	while(rslt.next()){
	    	    		num++;
	    	    	}
	    	    	
	    	    	//int pid = Integer.parseInt(rslt.getString(1).trim()) + 1;
	    	    	String pidStr = num + "";
	    	    	sql =  "INSERT INTO POST (PID, NOTE, SENDER, PLEVEL, PDATE) VALUES ('"+pidStr+"', '\""+ postandsearch.textArea.getText() +"\"', '"+ loginPanel.getUserName() +"', 1.0, to_date('"+ new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime()) +"', 'mm/dd/yyyy'))";
	    	    	stmt.executeQuery (sql);
		    	    sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
	    	    	JOptionPane.showMessageDialog(null, "Post succeeded!");     
            	}
            	catch(SQLException e1){
            		JOptionPane.showMessageDialog(null, "Post failed!");
            	     e1.printStackTrace();          
            	}
            	finally{
            	     try{
            	          if (conn != null){
            	               conn.close();
            	          }
            	     }
            	     catch(SQLException e1){
            	          e1.printStackTrace();          
            	     }
            	}
            	
            }
        });


		postandsearch.buttons[1].addActionListener(new ActionListener() {
           
            public void actionPerformed(ActionEvent e) {  
            	/*Fill this function*/
            	/*Press this Button, you should be able search user information, list the information on the result panel*/
            	/*The search should based on email, first name or last name*/
            	ConnectDB connect = new ConnectDB();
    	    	Connection conn = connect.openConnection();
    	    	try{
    	    		Statement stmt = conn.createStatement();
    	    		Scanner searchf = new Scanner(postandsearch.text.getText());
    	    		if(!searchf.hasNext()){
    	    			throw new NullPointerException();
    	    		}
    	    		
    	    		String searchfs = searchf.next();
    	    		String sql = "select u.email, u.fname, u.lname, u.birthdate, a.street_no, a.street_add, a.city, a.state, a.zip, a.country "
    	    				+ "from users u, address a "
    	    				+ "where (u.aid = a.aid) AND ((u.email = '" + searchfs + "') OR (u.fname = '" + searchfs + "') OR (u.lname = '" + searchfs + "'))";
    	    		
    	    		System.out.println(sql);
	    	    	ResultSet rslt = stmt.executeQuery(sql);
	    	    	sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
	    	    	String result = "";
	    	    	while (rslt.next()){
	    	    		for (int i = 1;i <= 3 ;i++){
	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    ";
	    	    		}
	    	    		//System.out.print (rslt.getMetaData().getColumnName(4) + ": " + rslt.getDate(4).toString().trim() + "    ");
	    	    		result = result + rslt.getMetaData().getColumnName(4) + ": " + rslt.getDate(4).toString().trim() + "    ";
	    	    		for (int i = 5;i <= 9;i++){
	    	    			//System.out.print (rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    "); 
	    	    			result = result + rslt.getMetaData().getColumnName(i) + ": " + rslt.getString(i).trim() + "    ";    	    			
	    	    		}
	    	    		result = result + rslt.getMetaData().getColumnName(10) + ": " + rslt.getString(10).trim() + "    \n";
	    	    		//System.out.println();
	    	    	}
	    	    	
	    	    	if(result.equals("")){
	            		JOptionPane.showMessageDialog(null, "This user doesn't exist!");		

	    	    	}
	    	    	resultArea.setText(result);
		    	    	
    	    	}
            	catch(SQLException e1){ 
	        		e1.printStackTrace(); 		
            	}
    	    	catch(NullPointerException e1){
            		//e1.printStackTrace();
            		JOptionPane.showMessageDialog(null, "Empty string is not allowed in the search box!");		
            	}
    	    	finally{
    	    		try{
    	    			if (conn != null){
    	    				conn.close();
    	    			}
    	    		}
    	    		catch(SQLException e1){ 
    	        		e1.printStackTrace(); 		
                	}
    	    	}
            	
            	
            	
            }
        });
	}
	public void setSQLOutput(StringBuffer sb)
	{
		sqlPanel.SQLArea.setText(sb.toString());
		sqlPanel.SQLArea.setEnabled(true);
	}
	public void setSqlPanel(){
		sqlPanel = new SqlPanel();
		this.add(sqlPanel);
		showLabel = new JLabel("The corresponding SQL :");
		showLabel.setBounds(20, 470, 200, 20);
		this.add(showLabel);
	}

	public void setLoginPanel(){
		loginPanel = new LoginPanel();
		this.add(loginPanel);

		loginPanel.signup.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
		        signUpPanel.enablePanel();
			}
        });
        loginPanel.login.addActionListener(new ActionListener() {
        	boolean found = false;
            public void actionPerformed(ActionEvent e) {  
            	/*Fill this function*/
            	/*Press this Button, you should be able match the user information. If valid, keep the user email information(but can't modified) and clear the password*/
            	/*at the same time, you should scan the friend request table, to determine whether current logged in user has friend request, if has, set notification icon*/
            	/*If invalid, you should pop up a dialog box to notify user, then enable signup panel for user to register*/
            	/*After logged in, you should change this button's function as logout which means disable all the panel, return to the original state*/
        		
        		ConnectDB connect = new ConnectDB();
    	    	Connection conn = connect.openConnection();
    	    	try{
    	    		if(found == false){
		            	Statement stmt = conn.createStatement();
		            	String sql =  "select EMAIL, PASSWD from USERS";
		    	    	ResultSet rslt = stmt.executeQuery (sql);
		    	    	sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
		    	    	while(rslt.next()){
		    	    		
		    	    		String dbUserName = rslt.getString(1).trim();
		    	    		String dbPassword = rslt.getString(2).trim();
		 
		    	    		if(loginPanel.getUserName().equals(dbUserName) && loginPanel.getPassword().equals(dbPassword)){
		    	    			System.out.println ("success");
		    	    			//loginPanel.clearPassword();
		    	    			//loginPanel.getPassword();
		    	    			signUpPanel.disablePanel();
		    	    			loginPanel.password.setText(null);
		    	    			sqlPanel.enablePanel();
		    	    			buttonPanel.enablePanel();
		    	    			postandsearch.enablePanel();
		    	    			found = true;
		    	    			/*
		    	    			stmt.executeQuery (
				    	    		      "INSERT INTO friendreq (sender, receiver) VALUES ('brad@csci585.edu', 'bill@csci585.edu')"
				    	    		    );
				    	    	*/
		    	    			break;
		    	    		}
		    	    	}
		    	    	if(found == false){
	    	    			signUpPanel.enablePanel();
	    	    			JOptionPane.showMessageDialog(null, "Wrong UserName or Password");	
	    	    			System.out.println ("fail");
		    	    	}
		    	    	
		    	    	/*at the same time, you should scan the friend request table, 
		    	    	 * to determine whether current logged in user has friend request, 
		    	    	 * if has, set notification icon*/
		    	    	else {
		    	    		ResultSet freq = stmt.executeQuery (
			    	    		      "select receiver from friendreq"
			    	    		    );
		    	    		//System.out.println ("select receiver from friendreq");
		    	    		while (freq.next()){
		    	    			String receiver = freq.getString(1).trim();
		    	    			//System.out.println (receiver);
		    	    			if (receiver.equals(loginPanel.getUserName())){
		    	    				notifyButton.setVisible(true);
			    	    			break;
		    	    			}
		    	    		}
		    	    	}
    	    	}
    	    	
    	    	else{
    	    		notifyButton.setVisible(false);
    	    		resultArea.setText(null);
    	    		postandsearch.text.setText(null);
    	    		sqlPanel.SQLArea.setText(null);
    	    		//loginPanel.clearUserName();
    	    		loginPanel.username.setText(null);
    	    		signUpPanel.disablePanel();
	    			sqlPanel.disablePanel();
	    			buttonPanel.disablePanel();
	    			postandsearch.disablePanel();
	    			found = false;
    	    	}
	    	    	//stmt.close();
	            	//loginPanel.getUserName();
	            	//loginPanel.getPassword();
            	}
            	catch(SQLException e1){ 
	        		e1.printStackTrace(); 		
            	}
    	    	finally{
    	    		try{
    	    			if (conn != null){
    	    				conn.close();
    	    			}
    	    		}
    	    		catch(SQLException e1){ 
    	        		e1.printStackTrace(); 		
                	}
    	    	}
	
            }
            

           });
   

	}

	public void setSignupPanel(){

		signUpPanel = new SignupPanel();
		this.add(signUpPanel);
		signUpPanel.signup.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {  
            	/*Fill this function*/
            	/*Press this signup button, you should be able check whether current account is existed. If existed, pop up an error, if not check input validation(You can design this part according to your database table's restriction) create the new account information*/
            	/*pop up a standard dialog box to show <succeed or failed>*/
            	
            	try{
        	    	ConnectDB connect = new ConnectDB();
        	    	Connection conn = connect.openConnection();
        	    	Statement stmt = conn.createStatement();
        	    	String sql = "INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) VALUES ('" 
        	    				+signUpPanel.email.getText() +"', '" 
        	    				+ signUpPanel.password.getText() +"', '" 
        	    				+ signUpPanel.fname.getText() + "', '" 
        	    				+ signUpPanel.lname.getText() + 
        	    				"', to_date('" + signUpPanel.birthday.getText() + "', 'mm/dd/yyyy'), '1')";
        	    	
        	    	//String sql1 = "INSERT INTO USERS (EMAIL, PASSWD, FNAME, LNAME, BIRTHDATE, AID) VALUES ('diy@usc.edu', '123456', 'Angelina', 'Jolie', to_date('04-Jun-1975', 'dd-mon-yyyy'), 'Uinted States', 'CA', 'Los Angeles')";
        	    	//System.out.println(sql);
        	    	//String sql2 = "INSERT INTO ADDRESS (AID, STREET_NO, STREET_ADD, CITY, STATE, COUNTRY, ZIP, COORD)VALUES ('"++"', '"++"', '"++"', '"++"', '"++"', '"++"', "++", '"++"')";
        	    	ResultSet rslt = stmt.executeQuery (
      	    		      "select EMAIL from USERS"
      	    		    );
        	    	
        	 	
        	    	while (rslt.next()){
        	    		String email = rslt.getString(1).trim();     	    		
        	    		if (signUpPanel.email.getText().equals(email)){
            	    		throw new IllegalArgumentException();
            	    	}		
        	    	}
        	    	

    	    	    if (!signUpPanel.password.getText().equals(signUpPanel.password2.getText())){
    	    			throw new InputMismatchException();
    	    		}
    	    	    
    	    	    Scanner strNo = new Scanner(signUpPanel.str_no.getText());
    	    	    Scanner zip = new Scanner(signUpPanel.zip.getText());
    	    	    Scanner email = new Scanner(signUpPanel.email.getText());
    	    	    Scanner password = new Scanner(signUpPanel.password.getText());
    	    	    Scanner password2 = new Scanner(signUpPanel.password2.getText());
    	    	    Scanner fname = new Scanner(signUpPanel.fname.getText());
    	    	    Scanner lname = new Scanner(signUpPanel.lname.getText());
    	    	    Scanner birthday = new Scanner(signUpPanel.birthday.getText());
    	    	    Scanner str_address = new Scanner(signUpPanel.str_address.getText());
    	    	    Scanner city = new Scanner(signUpPanel.city.getText());
    	    	    
    	    	    if ((!strNo.hasNext()) || (!zip.hasNext()) ||(!email.hasNext()) ||(!password.hasNext()) ||(!password2.hasNext()) ||(!fname.hasNext()) ||(!lname.hasNext()) ||(!birthday.hasNext()) ||(!str_address.hasNext()) ||(!city.hasNext())){
    	    	    	throw new NullPointerException();
    	    	    }
    	    	    
    	    	    if((!strNo.hasNextInt()) || (!zip.hasNextInt())){
    	    	    	throw new IllegalStateException();
    	    	    }
    	    
    	    	    
        	    	stmt.executeQuery (sql);
        	    	sqlPanel.SQLArea.setText(sqlAll += sql + "\n");
        	    	JOptionPane.showMessageDialog(null, "Succeed!");	
        	    	stmt.close();
        	    	  
            	}
            	
            	catch(SQLException e1){
            		e1.printStackTrace();
            		JOptionPane.showMessageDialog(null, "Invalid character!");		
            	}
            	
            	catch(NullPointerException e1){
            		//e1.printStackTrace();
            		JOptionPane.showMessageDialog(null, "No input field can remain unfilled!");		
            	}
            	catch(IllegalArgumentException e1){
            		JOptionPane.showMessageDialog(null, "This email has aready been signed up!");
            	}
            	
            	catch(IllegalStateException e1){
            		JOptionPane.showMessageDialog(null, "StrNo and Zip should be numbers!");
            	}
            	
            	catch(InputMismatchException e1){
            		JOptionPane.showMessageDialog(null, "Password doesn't match!");
            	}
            	finally{
    	    		try{
    	    			if (conn != null){
    	    				conn.close();
    	    			}
    	    		}
    	    		catch(SQLException e1){ 
    	        		e1.printStackTrace(); 		
                	}
            	}
            }
        });



	}


}


class ConnectDB{

	public static Connection openConnection(){
       
		/*Fill this function*/
    	/*implement open  connection */
		try{
	         String driverName = "oracle.jdbc.driver.OracleDriver";
	         Class.forName(driverName);
	         String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	         String uname = "scott";
	         String pwd = "diy";
	         return DriverManager.getConnection(url, uname, pwd);
	        }
	        catch(ClassNotFoundException e){
	         System.out.println("Class Not Found");
	         e.printStackTrace();
	         return null;
	        }
	        catch(SQLException sqle){
	         System.out.println("Connection Failed");
	         sqle.printStackTrace();
	         return null;
	        }
	 }
	 public static void closeConnection(Connection conn)
	 {
	 try{
	  conn.close();
	  }
	     catch (Exception e){
	      e.printStackTrace();
	      System.out.println("connection closing error ");
	     }
	}
	 

}
public class Assignment2 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	MainFrame frame = new MainFrame();
    	frame.setVisible(true);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	/*
    	try{
	    	ConnectDB connect = new ConnectDB();
	    	Connection conn = connect.openConnection();
	    	Statement stmt = conn.createStatement();
	    	ResultSet rset = stmt.executeQuery (
	    		      "select * from ADDRESS"
	    		    );
	    	
	    	while (rslt.next()){
	    		for (int i = 1;i <= rslt.getMetaData().getColumnCount() ;i++){
	    			System.out.print (rslt.getString(i)); // Print col 
	    		}
	    		System.out.println();
	    	}
	    	
	    	    stmt.close();
    	}
    	catch(SQLException e){ 
    			// TODO �Զ����� catch �� 
    		e.printStackTrace(); 		
    	}
    	*/
    	
	}
}
