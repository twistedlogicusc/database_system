
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
public class q3 {
    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);

        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            String line = value.toString();
            String[] SingleUserData = line.split(",");
            String ageGroup;
            int age = Integer.parseInt(SingleUserData[1]);
            if (age >= 5 && age <= 14) {
                ageGroup = "a";
                output.collect(new Text(ageGroup), one);
            }
            if (age >= 15 && age <= 24) {
                ageGroup = "b";
                output.collect(new Text(ageGroup), one);
            }
            if (age >= 25 && age <= 34) {
                ageGroup = "c";
                output.collect(new Text(ageGroup), one);
            }
            if (age >= 35 && age <= 44) {
                ageGroup = "d";
                output.collect(new Text(ageGroup), one);
            }
            if (age >= 45 && age <= 54) {
                ageGroup = "e";
                output.collect(new Text(ageGroup), one);
            }
            if (age >= 55) {
                ageGroup = "f";
                output.collect(new Text(ageGroup), one);
            }
        }
    }

    public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            int sum = 0;
            while (values.hasNext()) {
                sum += values.next().get();
            }
            output.collect(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(q3.class);
        conf.setJobName("q3");

        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);

        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
    }



}
