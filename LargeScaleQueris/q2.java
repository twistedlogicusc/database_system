
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
public class q2 {
    public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);

        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            String line = value.toString();
            String[] SingleUserData = line.split(",");
            String interest = SingleUserData[2];
            double distance = 6;
            double x = Double.parseDouble(SingleUserData[3]);
            double y = Double.parseDouble(SingleUserData[4]);
            if(interest.equals("Aircraft")){
                distance = Math.sqrt((x - 56) * (x - 56) + (y - 74) * (y - 74));
            }
            if(interest.equals("Art antique collecting")){
                distance = Math.sqrt((x - 93) * (x - 93) + (y - 39) * (y - 39));
            }
            if(interest.equals("Astrology")){
                distance = Math.sqrt((x - 60) * (x - 60) + (y - 1) * (y - 1));
            }
            if(interest.equals("Baseball")){
                distance = Math.sqrt((x - 36) * (x - 36) + (y - 85) * (y - 85));
            }
            if(interest.equals("Basketball")){
                distance = Math.sqrt((x - 60) * (x - 60) + (y - 49) * (y - 49));
            }
            if(interest.equals("Bicycling")){
                distance = Math.sqrt((x - 82) * (x - 82) + (y - 59) * (y - 59));
            }
            if(interest.equals("Boating sailing")){
                distance = Math.sqrt((x - 86) * (x - 86) + (y - 22) * (y - 22));
            }
            if(interest.equals("Book reading")){
                distance = Math.sqrt((x - 31) * (x - 31) + (y - 23) * (y - 23));
            }
            if(interest.equals("Camping hiking")){
                distance = Math.sqrt((x - 22) * (x - 22) + (y - 73) * (y - 73));
            }
            if(interest.equals("Casino vacation")){
                distance = Math.sqrt((x - 34) * (x - 34) + (y - 25) * (y - 25));
            }
            if(interest.equals("Clothing")){
                distance = Math.sqrt((x - 33) * (x - 33) + (y - 98) * (y - 98));
            }
            if(interest.equals("Cooking")){
                distance = Math.sqrt((x - 33) * (x - 33) + (y - 78) * (y - 78));
            }
            if(interest.equals("Cosmetics")){
                distance = Math.sqrt((x - 53) * (x - 53) + (y - 85) * (y - 85));
            }
            if(interest.equals("Cruise vacation")){
                distance = Math.sqrt((x - 79) * (x - 79) + (y - 50) * (y - 50));
            }
            if(interest.equals("Electronics")){
                distance = Math.sqrt((x - 13) * (x - 13) + (y - 69) * (y - 69));
            }
            if(interest.equals("Fashion")){
                distance = Math.sqrt((x - 48) * (x - 48) + (y - 79) * (y - 79));
            }
            if(interest.equals("Fishing")){
                distance = Math.sqrt((x - 88) * (x - 88) + (y - 33) * (y - 33));
            }
            if(interest.equals("Fitness")){
                distance = Math.sqrt((x - 4) * (x - 4) + (y - 22) * (y - 22));
            }
            if(interest.equals("Football")){
                distance = Math.sqrt((x - 90) * (x - 90) + (y - 32) * (y - 32));
            }
            if(interest.equals("Golf")){
                distance = Math.sqrt((x - 33) * (x - 33) + (y - 55) * (y - 55));
            }
            if(interest.equals("History")){
                distance = Math.sqrt((x - 32) * (x - 32) + (y - 70) * (y - 70));
            }
            if(interest.equals("Hockey")){
                distance = Math.sqrt((x - 67) * (x - 67) + (y - 75) * (y - 75));
            }
            if(interest.equals("Home furnishings")){
                distance = Math.sqrt((x - 19) * (x - 19) + (y - 14) * (y - 14));
            }
            if(interest.equals("Household pets")){
                distance = Math.sqrt((x - 9) * (x - 9) + (y - 91) * (y - 91));
            }
            if(interest.equals("Hunting")){
                distance = Math.sqrt((x - 81) * (x - 81) + (y - 63) * (y - 63));
            }
            if(interest.equals("Jewelry")){
                distance = Math.sqrt((x - 52) * (x - 52) + (y - 35) * (y - 35));
            }
            if(interest.equals("Scuba")){
                distance = Math.sqrt((x - 9) * (x - 9) + (y - 56) * (y - 56));
            }
            if(interest.equals("Shoes")){
                distance = Math.sqrt((x - 40) * (x - 40) + (y - 58) * (y - 58));
            }
            if(interest.equals("Swimming pool")){
                distance = Math.sqrt((x - 41) * (x - 41) + (y - 14) * (y - 14));
            }
            if(interest.equals("Tennis")){
                distance = Math.sqrt((x - 85) * (x - 85) + (y - 74) * (y - 74));
            }

            if(distance <= 5.0){
                output.collect(new Text(interest), one);
            }
        }
    }

    public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            int sum = 0;
            while (values.hasNext()) {
                sum += values.next().get();
            }
            output.collect(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(q2.class);
        conf.setJobName("q2");

        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);

        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
    }



}